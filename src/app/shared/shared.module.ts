import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheatModalComponent } from './cheat-modules/cheat-modal/components/cheat-modal/cheat-modal.component';
import { CheatBtnModule } from './cheat-modules/cheat-btn/cheat-btn.module';
import { LoginComponent } from '../auth/components/login/login.component';
import { CheatModalModule } from './cheat-modules/cheat-modal/cheat-modal.module';

@NgModule({
    imports: [
        CommonModule,
        CheatModalModule.config({
            componentsUsedInsideModal: [LoginComponent]
        }),
        CheatBtnModule
    ],
    declarations: [],
    exports: [],
    entryComponents: [
        CheatModalComponent
    ],
    providers: []
})
export class SharedModule {
}
