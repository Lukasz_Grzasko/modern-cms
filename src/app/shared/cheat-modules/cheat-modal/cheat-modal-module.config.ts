import { InjectionToken } from '@angular/core';

export interface CheatModalModuleConfig {
    /**
     * When you want to pass the custom modal header, body or footer as your custom component, you have to declare them in the config.
     * That components will be added to 'entryComponents' array of the CheatModalModule. It is necessary bacuse of Angular mechanism.
     * When you want to use dynamically created components, Angular requires add them to the 'entryComponents' array in the module declaration.
     */
    componentsUsedInsideModal: any[];
}

export const CHEAT_MODAL_MODULE_CONFIG = new InjectionToken<CheatModalModuleConfig>('CHEAT_MODAL_MODULE_CONFIG');
