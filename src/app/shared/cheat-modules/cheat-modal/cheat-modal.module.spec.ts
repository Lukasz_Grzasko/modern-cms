import { CheatModalModule } from './cheat-modal.module';

describe('CheatModalModule', () => {
    let cheatModalModule: CheatModalModule;

    beforeEach(() => {
        cheatModalModule = new CheatModalModule();
    });

    it('should create an instance', () => {
        expect(cheatModalModule).toBeTruthy();
    });
});
