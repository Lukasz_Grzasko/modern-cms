import { ANALYZE_FOR_ENTRY_COMPONENTS, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CHEAT_MODAL_MODULE_CONFIG, CheatModalModuleConfig } from './cheat-modal-module.config';
import { CheatModalService } from './services/cheat-modal.service';
import { CheatModalInstanceService } from './services/cheat-modal-instance.service';
import { CheatModalComponent } from './components/cheat-modal/cheat-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CheatBtnModule } from '../cheat-btn/cheat-btn.module';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        CheatBtnModule
    ],
    declarations: [
        CheatModalComponent
    ]
})
export class CheatModalModule {

    static config(config: CheatModalModuleConfig): ModuleWithProviders {
        return {
            ngModule: CheatModalModule,
            providers: [
                CheatModalService,
                CheatModalInstanceService,
                {provide: CHEAT_MODAL_MODULE_CONFIG, useValue: config},
                {provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: config.componentsUsedInsideModal, multi: true},
            ]
        };
    }

}
