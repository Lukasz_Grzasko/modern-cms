export interface CheatModalConfig {
    title?: string;
    closePossibility?: boolean;
    closeBtn?: boolean;
    bodyComponent?: any;
}
