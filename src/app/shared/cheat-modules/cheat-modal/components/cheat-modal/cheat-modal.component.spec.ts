import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheatModalComponent } from './cheat-modal.component';

describe('CheatModalComponent', () => {
    let component: CheatModalComponent;
    let fixture: ComponentFixture<CheatModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CheatModalComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CheatModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
