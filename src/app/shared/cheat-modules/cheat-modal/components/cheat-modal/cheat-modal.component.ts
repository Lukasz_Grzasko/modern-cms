import {
    AfterContentInit, Component, ComponentFactoryResolver, ComponentRef, HostBinding, OnInit, ViewChild,
    ViewContainerRef,
    ViewEncapsulation
} from '@angular/core';
import { CheatModalInstanceService } from '../../services/cheat-modal-instance.service';
import { CheatModalConfig } from '../../interfaces/cheat-modal-config.interface';
import { cheatModalFade } from '../../animations/cheat-modal-fade.animation';

declare const System;

@Component({
    selector: 'cheat-modal',
    templateUrl: './cheat-modal.component.html',
    styleUrls: ['./cheat-modal.component.scss'],
    // encapsulation: ViewEncapsulation.None,
    animations: [cheatModalFade]
})
export class CheatModalComponent implements OnInit, AfterContentInit {

    @HostBinding('@cheatModalFade') cheatModalFade: string;

    @ViewChild('body', {read: ViewContainerRef}) body: ViewContainerRef;

    config: CheatModalConfig;
    bodyComponentRef: ComponentRef<any>;

    constructor(private cheatModalInstanceService: CheatModalInstanceService,
                private resolver: ComponentFactoryResolver) {
    }

    ngOnInit() {
        this.config = this.cheatModalInstanceService.getConfig();
    }

    ngAfterContentInit() {
        const bodyComponentFactory = this.resolver.resolveComponentFactory(this.config.bodyComponent);
        this.bodyComponentRef = this.body.createComponent(bodyComponentFactory);
    }

    close() {
        // On modal close, destroy component that was passed to the body section.
        // It is necessary to unbind all data from that component.
        this.bodyComponentRef.destroy();
        this.cheatModalInstanceService.close();
    }

}
