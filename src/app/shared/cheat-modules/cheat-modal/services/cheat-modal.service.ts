import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { CheatModalComponent } from '../components/cheat-modal/cheat-modal.component';
import { AppService } from '../../../../app.service';
import { CheatModalInstanceService } from './cheat-modal-instance.service';
import { CheatModalConfig } from '../interfaces/cheat-modal-config.interface';

@Injectable({
    providedIn: 'root'
})
export class CheatModalService {

    constructor(private resolver: ComponentFactoryResolver,
                private cheatModalInstanceService: CheatModalInstanceService,
                private appService: AppService) {
    }

    /**
     * Inits a logic and view for the newly opened modal.
     * @param {CheatModalConfig} config -
     */
    open(config: CheatModalConfig): void {

        const
            // First, create dynamically a modal component and place it in a previously declared container.
            appViewRef = this.appService.getAppViewRef(),
            factory = this.resolver.resolveComponentFactory(CheatModalComponent),
            component = appViewRef.createComponent(factory);

        this.cheatModalInstanceService.saveNewlyCreatedDialog(appViewRef, component, config);
    }

    /**
     * A function that mediates when closing a modal.
     * It tells the next modal service to close it.
     */
    close(): void {
        this.cheatModalInstanceService.close();
    }

}
