import { TestBed, inject } from '@angular/core/testing';

import { CheatModalInstanceService } from './cheat-modal-instance.service';

describe('ModalInstanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheatModalInstanceService]
    });
  });

  it('should be created', inject([CheatModalInstanceService], (service: CheatModalInstanceService) => {
    expect(service).toBeTruthy();
  }));
});
