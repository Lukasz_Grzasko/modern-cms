import { TestBed, inject } from '@angular/core/testing';

import { CheatModalService } from './cheat-modal.service';

describe('ModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheatModalService]
    });
  });

  it('should be created', inject([CheatModalService], (service: CheatModalService) => {
    expect(service).toBeTruthy();
  }));
});
