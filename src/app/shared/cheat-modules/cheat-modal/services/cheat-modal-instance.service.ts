import { ComponentRef, Injectable, Renderer2, RendererFactory2, ViewContainerRef } from '@angular/core';
import { CheatModalComponent } from '../components/cheat-modal/cheat-modal.component';
import { CheatModalConfig } from '../interfaces/cheat-modal-config.interface';

@Injectable({
    providedIn: 'root'
})
export class CheatModalInstanceService {

    appViewRef: ViewContainerRef;
    dialogRef: ComponentRef<CheatModalComponent>;
    renderer: Renderer2;

    defaultConfig: CheatModalConfig = this.createConfig();
    config: CheatModalConfig;

    constructor(private rendererFactory: RendererFactory2) {
        this.renderer = this.rendererFactory.createRenderer(null, null);
        this.defaultConfig = this.createConfig();
    }

    /**
     * Creates an object with a default config to a modal.
     * @return {CheatModalConfig} - Default config.
     */
    createConfig(): CheatModalConfig {
        return {
            closePossibility: true,
            closeBtn: true,
            bodyComponent: null
        };
    }

    /**
     *
     * @param {ViewContainerRef} appViewRef - Container the dialog should be placed in the DOM structure.
     * @param {ComponentRef<CheatModalComponent>} dialogRef - Dialog handle in the DOM structure.
     * @param {CheatModalConfig} config - Custom config provided by a user.
     */
    saveNewlyCreatedDialog(appViewRef: ViewContainerRef,
                           dialogRef: ComponentRef<CheatModalComponent>,
                           config: CheatModalConfig) {

        this.appViewRef = appViewRef;
        this.dialogRef = dialogRef;

        this.config = Object.assign({}, this.defaultConfig, config);

        this.buildBody();

        this.open();
    }

    /**
     * Opens a modal by setting all necesary UI styles.
     */
    open(): void {
        this.renderer.setStyle(document.body, 'overflow', 'hidden');
       // During 'overflow: hidden', the scrollbar disappears and the view jumps to the right.
       // Setting a margin like the width of the scrollbar prevents this unpleasant effect.
        this.renderer.setStyle(document.body, 'margin-right', '15px');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'transition-property', 'filter, opacity');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'transition-duration', '300ms');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'filter', 'blur(.35rem)');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'opacity', '.35');
    }

    close(): void {
        this.renderer.setStyle(document.body, 'overflow', '');
        this.renderer.setStyle(document.body, 'margin-right', '');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'filter', 'blur(0)');
        this.renderer.setStyle(this.appViewRef.element.nativeElement, 'opacity', '1');
        this.dialogRef.destroy();
    }

    getConfig(): CheatModalConfig {
        return this.config;
    }

    buildBody() {
    }


}
