import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheatBtnComponent } from './components/cheat-btn/cheat-btn.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        CheatBtnComponent
    ],
    exports: [
        CheatBtnComponent
    ]
})
export class CheatBtnModule {
}
