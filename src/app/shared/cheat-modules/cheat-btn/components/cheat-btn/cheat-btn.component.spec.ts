import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheatBtnComponent } from './cheat-btn.component';

describe('CheatBtnComponent', () => {
    let component: CheatBtnComponent;
    let fixture: ComponentFixture<CheatBtnComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CheatBtnComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CheatBtnComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
