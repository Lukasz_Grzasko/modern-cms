import { ViewContainerRef, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AppService {

    appViewRef: ViewContainerRef;

    constructor() {
    }

    setAppViewRef(appViewRef: ViewContainerRef): void {
        this.appViewRef = appViewRef;
    }

    getAppViewRef(): ViewContainerRef {
        return this.appViewRef;
    }

}
