import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        SidebarComponent,
        NavbarComponent,
        DashboardComponent
    ],
    exports: [
        SidebarComponent,
        NavbarComponent
    ]
})
export class CoreModule {
}
