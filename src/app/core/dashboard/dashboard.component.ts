import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../auth/store';
import { Observable } from 'rxjs/index';
import { AuthState } from '../../auth/store/reducers/index';

@Component({
    selector: 'm-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    login$: Observable<AuthState>;


    constructor(private store: Store<fromStore.AuthState>) {
    }

    ngOnInit() {
        this.login$ = this.store.select(fromStore.getAuthState);
    }

}
