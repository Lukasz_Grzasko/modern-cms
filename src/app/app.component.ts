import { AfterContentInit, Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { CheatModalService } from './shared/cheat-modules/cheat-modal/services/cheat-modal.service';
import { AppService } from './app.service';
import { LoginComponent } from './auth/components/login/login.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterContentInit {

    @ViewChild('app', {read: ViewContainerRef}) app: ViewContainerRef;

    constructor(private appService: AppService,
                private cheatModalService: CheatModalService) {
    }

    ngOnInit() {
    }

    ngAfterContentInit() {
        this.appService.setAppViewRef(this.app);
    }

    openModal() {
        this.cheatModalService.open({
            closeBtn: false,
            bodyComponent: LoginComponent
        });
    }

}
