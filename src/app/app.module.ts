import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './core/dashboard/dashboard.component';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/guards/auth.guard';
import { SharedModule } from './shared/shared.module';
import { AppService } from './app.service';
import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './auth/interceptors/auth.interceptor';

export const metaReducers: MetaReducer<any>[] = [storeFreeze];

const ROUTES: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            {path: '', component: DashboardComponent},
            {path: '**', redirectTo: ''}
        ]
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        StoreDevtoolsModule.instrument(),
        StoreModule.forRoot({}, {metaReducers}),
        EffectsModule.forRoot([]),
        CoreModule,
        AuthModule,
        RouterModule.forRoot(ROUTES),
        SharedModule,
        HttpClientModule
    ],
    providers: [
        AuthGuard,
        AppService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
