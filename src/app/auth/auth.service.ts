import { Injectable } from '@angular/core';
import { LoginData } from './interfaces/login-data.interface';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LoginResponse } from './interfaces/login-response.interface';
import { User } from './interfaces/user.interface';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    login(payload: LoginData): Observable<LoginResponse> {
        return this.http
            .post(environment.apiUrl + '/auth/login', payload)
            .pipe(
                map((res: LoginResponse) => {
                    localStorage.setItem('access_token', res.access_token);
                    return res;
                })
            );
    }

    getUser(): Observable<User> {
        return this.http
            .get(environment.apiUrl + '/user')
            .pipe(
                map((res: User) => res)
            );
    }

}
