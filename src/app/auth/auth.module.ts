import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CheatBtnModule } from '../shared/cheat-modules/cheat-btn/cheat-btn.module';
import { StoreModule } from '@ngrx/store';
import { reducers, effects } from './store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        CheatBtnModule,
        StoreModule.forFeature('auth', reducers),
        EffectsModule.forFeature(effects)
    ],
    declarations: [LoginComponent]
})
export class AuthModule {
}
