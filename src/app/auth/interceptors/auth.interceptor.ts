import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // Get token from local storage.
        const token = localStorage.getItem('access_token');

        // If the token is preset, append it to a Http request.
        if (token) {

            // A request is an immutable object, so clone it instead of changing the properties.
            const cloned = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${ token }`)
            });

            return next.handle(cloned);
        } else {
            // If token isn't present, do nothing and simply continue the middleware chain.
            return next.handle(req);
        }
    }

}