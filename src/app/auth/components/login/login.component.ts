import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { Subscription } from 'rxjs/index';
import { CheatModalService } from '../../../shared/cheat-modules/cheat-modal/services/cheat-modal.service';

@Component({
    selector: 'm-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    form: FormGroup;

    loginData$: Subscription;

    constructor(private fb: FormBuilder,
                private store: Store<fromStore.AuthState>,
                private cheatModalService: CheatModalService) {

        this.form = this.createForm();
    }

    ngOnInit() {
        this.loginData$ = this.store.select(fromStore.getLoginLoaded)
            .subscribe(isAuthenticated => isAuthenticated && this.cheatModalService.close());
    }

    createForm(): FormGroup {
        return this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    login(): void {
        const payload = {
            email: this.form.get('email').value,
            password: this.form.get('password').value
        };

        this.store.dispatch(new fromStore.Login(payload));
    }

}
