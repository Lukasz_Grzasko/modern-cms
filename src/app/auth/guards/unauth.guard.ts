import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UnauthGuard implements CanActivate {

    constructor(private router: Router) {
    }


    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const auth = localStorage.getItem('user');

        if (auth) {
            this.router.navigate(['']);
            return false;
        }

        return true;
    }

}
