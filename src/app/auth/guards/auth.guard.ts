import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CheatModalService } from '../../shared/cheat-modules/cheat-modal/services/cheat-modal.service';
import { CheatModalConfig } from '../../shared/cheat-modules/cheat-modal/interfaces/cheat-modal-config.interface';
import { LoginComponent } from '../components/login/login.component';

import * as fromStore from '../store';
import { Store } from '@ngrx/store';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    modalConfig: CheatModalConfig;

    /**
     * An idea of this guard is to open the modal if a user is unauthorized.
     * Not to log out, not to redirect to a login page. Only open the popup.
     */
    constructor(private cheatModalService: CheatModalService,
                private store: Store<fromStore.AuthState>) {

        this.modalConfig = this.getModalConfig();
    }

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): boolean {

        // Check if JWT token exists in local storage.
        const token = localStorage.getItem('access_token');

        if (!token) {

            // If not, open the auth popup.
            this.cheatModalService.open(this.modalConfig);

            // If the user is on '/' (dashboard), allow him to enter the route.
            // In another case, return false and don't change the route.
            return state.url === '/';
        }

        // If token exists, add it to the store.
        this.store.dispatch(new fromStore.LoginSuccess({access_token: token}));

        // And allow a user enter the page.
        return true;
    }

    /**
     * Helper function in order to generate a config for the auth modal.
     * @return {CheatModalConfig} - Config for the auth modal.
     */
    getModalConfig(): CheatModalConfig {
        return {
            title: 'Logowanie',
            closePossibility: false,
            closeBtn: false,
            bodyComponent: LoginComponent
        };
    }

}
