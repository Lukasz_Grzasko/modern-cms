import { Action } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';

export const GET_USER = '[Auth] Get user';
export const GET_USER_SUCCESS = '[Auth] Get user success';
export const GET_USER_FAILURE = '[Auth] Get user failure';

export class GetUser implements Action {
    readonly type = GET_USER;
}

export class GetUserSuccess implements Action {
    readonly type = GET_USER_SUCCESS;

    constructor(public payload: any) {
    }
}

export type GetUserAction = GetUser | GetUserSuccess;
