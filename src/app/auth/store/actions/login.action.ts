import { Action } from '@ngrx/store';
import { LoginData } from '../../interfaces/login-data.interface';

export const LOGIN = '[Auth] Login';
export const LOGIN_SUCCESS = '[Auth] Login success';
export const LOGIN_FAILURE = '[Auth] Login fail';

export class Login implements Action {
    readonly type = LOGIN;

    constructor(public payload: LoginData) {
    }
}

export class LoginSuccess implements Action {
    readonly type = LOGIN_SUCCESS;

    constructor(public payload: any) {
    }
}

export class LoginFailure implements Action {
    readonly type = LOGIN_FAILURE;

    constructor(public payload: any) {
    }
}

export type LoginAction = Login | LoginSuccess | LoginFailure;
