import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { AuthService } from '../../auth.service';
import * as authActions from '../actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { User } from '../../interfaces/user.interface';

@Injectable()
export class GetUserEffects {

    constructor(private actions$: Actions,
                private authService: AuthService) {
    }

    @Effect()
    getUser$ = this.actions$.ofType(authActions.GET_USER)
        .pipe(
            switchMap((action: authActions.Login) => {
                return this.authService
                    .getUser()
                    .pipe(
                        map((user: User) => new authActions.GetUserSuccess(user))
                    );
            })
        );

}
