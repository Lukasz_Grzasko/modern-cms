import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import * as authActions from '../actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { AuthService } from '../../auth.service';
import { LoginResponse } from '../../interfaces/login-response.interface';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import * as authStore from '../';

@Injectable()
export class LoginEffects {
    constructor(private actions$: Actions,
                private store: Store<authStore.AuthState>,
                private authService: AuthService) {
    }

    @Effect()
    login$ = this.actions$.ofType(authActions.LOGIN)
        .pipe(
            switchMap((action: authActions.Login) => {
                return this.authService
                    .login(action.payload)
                    .pipe(
                        map((token: LoginResponse) => {
                            this.store.dispatch(new authActions.GetUser());
                            return new authActions.LoginSuccess(token);
                        }),
                        catchError(err => of(new authActions.LoginFailure(err)))
                    );
            })
        );

}
