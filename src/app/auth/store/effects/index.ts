import { LoginEffects } from './login.effects';
import { GetUserEffects } from './get-user.effects';

export const effects: any[] = [
    LoginEffects,
    GetUserEffects
];

export * from './login.effects';
export * from './get-user.effects';
