import * as loginReducers from './login.reducer';
import * as getUserReducers from './get-user.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface AuthState {
    login: loginReducers.LoginState;
    getUser: getUserReducers.GetUserState;
}

export const reducers: ActionReducerMap<AuthState> = {
    login: loginReducers.reducer,
    getUser: getUserReducers.reducer,
};

export const getAuthState = createFeatureSelector<AuthState>(
    'auth'
);

export const getLoginState = createSelector(
    getAuthState,
    (state: AuthState) => state.login
);

export const getLoginData = createSelector(
    getLoginState,
    loginReducers.getLogin
);

export const getLoginLoading = createSelector(
    getLoginState,
    loginReducers.getLoginLoading
);

export const getLoginLoaded = createSelector(
    getLoginState,
    loginReducers.getLoginLoaded
);
