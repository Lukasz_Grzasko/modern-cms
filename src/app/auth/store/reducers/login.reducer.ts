import { User } from '../../interfaces/user.interface';
import * as fromLogin from '../actions/login.action';
import { LoginResponse } from '../../interfaces/login-response.interface';

export interface LoginState {
    data: LoginResponse;
    loaded: boolean;
    loading: boolean;
}

export const initialState: LoginState = {
    data: {access_token: ''},
    loaded: false,
    loading: false
};

export function reducer(state = initialState,
                        action: fromLogin.LoginAction): LoginState {

    switch (action.type) {

        case fromLogin.LOGIN: {
            return {
                ...state,
                loading: true
            };
        }

        case fromLogin.LOGIN_SUCCESS: {
            const data = action.payload;
            return {
                ...state,
                loading: false,
                loaded: true,
                data
            };
        }

        case fromLogin.LOGIN_FAILURE: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }
    }

    return state;
}

export const getLoginLoading = (state: LoginState) => state.loading;
export const getLoginLoaded = (state: LoginState) => state.loaded;
export const getLogin = (state: LoginState) => state.data;
