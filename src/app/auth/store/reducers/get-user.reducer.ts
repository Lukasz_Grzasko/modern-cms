import { User } from '../../interfaces/user.interface';
import * as authActions from '../actions';

export interface GetUserState {
    data: User;
    loaded: boolean;
    loading: boolean;
}

export const initialState: GetUserState = {
    data: {
        id: 0,
        email: '',
        name: ''
    },
    loaded: false,
    loading: false
};

export function reducer(state = initialState,
                        action: authActions.GetUserAction): GetUserState {

    switch (action.type) {

        case authActions.GET_USER: {
            return {
                ...state,
                loading: true
            }
        }

        case authActions.GET_USER_SUCCESS: {
            const data = action.payload;
            return {
                ...state,
                loading: false,
                loaded: true,
                data
            };
        }

    }

}
